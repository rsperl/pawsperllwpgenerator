(function() {
    var PerlLwpGenerator;

    PerlLwpGenerator = function() {
        this.request = function(paw_request) {
            var headers = [];
            var is_json = false;
            var ref = paw_request.headers;
            var key;
            var value;

            for (key in ref) {
                value = ref[key];
                if (key === 'Content-Type') {
                    is_json = value.search(/(json)/i) > -1;
                    continue;
                }
                headers.push({
                    key: key,
                    value: value
                });
            }
            var body = paw_request.body;

            if (body.length && is_json) {
                body = JSON.stringify(JSON.parse(body), null, 2);
            }

            return {
                headers: headers,
                body: body
            };
        };
        this.generate = function(context) {
            var paw_request = context.getCurrentRequest();
            var request = this.request(paw_request);

            var result = "";
            result += "use LWP;\n";
            result += "\n";
            result += "my $ua = LWP::UserAgent->new;\n";
            result += "\n";
            result += "my $req = HTTP::Request->new(" + paw_request.method + " => \"" + paw_request.url + "\");\n";

            if (request.headers.length) {
                result += '\n\n' + request.headers.map(function (header) {
                        return '$req->header("' + header.key + '" => ' + '"' + header.value + '");';
                        }).join('\n');
                result += "\n";
            }
            if (request.body.length) {
                result += '\n\n' + request.body;
            }

            result += "my $res = $ua->request($req);\n";
            result += "\n";
            result += 'print "status code: " . $res->code . "\\n";\n'
            result += 'print "status msg:  " . $res->message . "\\n";\n'
            result += "if( $res->is_success ) {\n";
            result += "    print $res->content;\n";
            result += "} else {\n";
            result += "    print $res->status_line . \"\\n\";\n";
            result += "}\n";

            return result;
        };
    };

    PerlLwpGenerator.identifier = "com.risugg.PerlLwpGenerator";
    PerlLwpGenerator.title = "Perl LWP Generator";
    PerlLwpGenerator.fileExtension = "pl";
    PerlLwpGenerator.languageHighlighter = "perl";

    registerCodeGenerator(PerlLwpGenerator);

}).call(this);
